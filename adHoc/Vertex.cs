﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace adHoc
{
    public abstract class Vertex
    {
        public abstract bool Intersects(Vertex other);
    }
    public class Vertex2D : Vertex
    {
        public readonly double X, Y, Radius;
        public override bool Intersects(Vertex other)
        {
            if (!(other is Vertex2D))
            {
                return false;
            }
            var v = other as Vertex2D;
            Func<double, double> Sqr = x => x * x;
            var distance = Math.Sqrt(Sqr(v.X - X) + Sqr(v.Y - Y));
            return (distance <= this.Radius + v.Radius);
        }
        public Vertex2D(double x, double y, double radius)
        {
            X = x;
            Y = y;
            Radius = radius;
        }
    }
    public class Vertex3D : Vertex
    {
        public readonly double X, Y, Z, Radius;
        public override bool Intersects(Vertex other)
        {
            if (!(other is Vertex3D))
            {
                return false;
            }
            var v = other as Vertex3D;
            Func<double, double> Sqr = x => x * x;
            var distance = Math.Sqrt(Sqr(v.X - X) + Sqr(v.Y - Y) + Sqr(v.Z - Z));
            return (distance <= this.Radius + v.Radius);
        }
        public Vertex3D(double x, double y, double z, double radius)
        {
            X = x;
            Y = y;
            Z = z;
            Radius = radius;
        }

    }
}
