﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace adHoc
{
    public class Graph
    {
        private int[,] dist;
        private int[] ecc;

        private const int inf = 100000;
        private int n;
        public Graph(int vertexCount)
        {
            n = vertexCount;
            dist = new int[n, n];
            ecc = new int[n];
        }
        public int[] DistancesFrom(int[][] G, int s)
        {
            var used = new bool[n];
            var dist = new int[n];
            for (int i = 0; i < n; i++)
            {
                used[i] = false;
                dist[i] = 0;
            }
            used[s] = true;
            var q = new Queue<int>();
            q.Enqueue(s);
            while (q.Count() > 0)
            {
                int v = q.Dequeue();
                foreach(var dest in G[v])
                {
                    if (!used[dest])
                    {
                        used[dest] = true;
                        q.Enqueue(dest);
                        dist[dest] = dist[v] + 1;
                    }
                }
            }
            return dist;
        }
        public int Radius(int[][] G)
        {
            return Enumerable
                .Range(0, G.Count() - 1)
                .Select(v => DistancesFrom(G, v).Max()) // эксцентриситет
                .Min();
        }
        //public int Radius(int[][] G)
        //{
        //    dist[0, 0] = 0;
        //    for (int i = 1; i < n; i++)
        //    {
        //        dist[i, i] = 0;
        //        for(int j = 0; j < i; j++)
        //        {
        //            dist[j, i] = dist[i, j] = G[i - 1][j] == 0 ? inf : G[i - 1][j];
        //        }
        //    }
        //    //System.Windows.Forms.MessageBox.Show(dist.Cast<int>().Aggregate("", (acc, cur) => acc + cur.ToString() + Environment.NewLine));            
        //    for (int k = 0; k < n; k++)
        //    {
        //        for (int j = 0; j < n; j++)
        //        {
        //            for (int i = 0; i < n; i++)
        //            {
        //                dist[i, j] = Math.Min(dist[i, j], dist[i, k] + dist[k, j]);
        //            }
        //        }
        //    }
        //    //System.Windows.Forms.MessageBox.Show(dist.Cast<int>().Aggregate("", (acc, cur) => acc + cur.ToString() + Environment.NewLine));            
        //    int min = inf;
        //    for (int i = 0; i < n; i++)
        //    {
        //        ecc[i] = 0;
        //        for (int j = 0; j < n; j++)
        //        {
        //            ecc[i] = Math.Max(ecc[i], dist[i, j]);
        //        }
        //        if (ecc[i] < min)
        //            min = ecc[i];
        //    }
        //    //var min = ecc.Min();
        //    //System.Windows.Forms.MessageBox.Show(ecc.Aggregate("", (acc, cur) => acc + cur.ToString() + Environment.NewLine));            
            
        //    // Мне кажется, радиус равный 100 000 это перебор :)
        //    return (min == inf) ? 0 : min;
        //}
    }
}
