﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using System.Security.Cryptography;

namespace adHoc
{
    public class NetworkGenerator
    {
        private static Random random = new Random();
        private Vertex[] vertices;
        private double RadiusMax;
        public Vertex CreateVertex(double radius, bool is3D)
        {
            var r = random.NextDouble() * RadiusMax;
            var phi = random.NextDouble() * 2.0 * Math.PI;                
            if(is3D)
            {
                var theta = random.NextDouble() * 2.0 * Math.PI;
                return new Vertex3D(r * Math.Sin(phi) * Math.Cos(theta), r * Math.Sin(phi) * Math.Sin(theta), r * Math.Cos(theta), radius);
            }
            else
            {
                return new Vertex2D(r * Math.Cos(phi), r * Math.Sin(phi), radius);
            }
        }
        /// <summary>
        /// Создает класс-генератор сети
        /// </summary>
        /// <param name="radiusMax">Максимальный радиус круга/сферы</param>
        /// <param name="count">Количество узлов сети</param>
        public NetworkGenerator(double radiusMax, int count)
        {
            RadiusMax = radiusMax;
            vertices = new Vertex[count];
        }
        /// <summary>
        /// Сгенерировать ad-hoc сеть и матрицу смежности
        /// </summary>
        /// <param name="count">Кол-во вершин сети</param>
        /// <param name="radiusMin">Минимальный радиус узла</param>
        /// <param name="radiusMax">Максимальный радиус узла</param>
        /// <param name="is3D">Если <c>true</c>, генерация идет в пространстве, иначе на плоскости</param>
        /// <returns>Матрица смежности в виде массива</returns>
        public int[][] Generate(double radiusMin, double radiusMax, bool is3D)
        {
            int count = vertices.Count();
            for (int i = 0; i < count; i++)
            {
                double radius = random.NextDouble() * (radiusMax - radiusMin) + radiusMin;
                vertices[i] = CreateVertex(radius, is3D);
            }
            var result = new int[count][];
            for (int i = 0; i < count; i++)
            {
                var lst = new List<int>();
                for (int j = 0; j < i; j++)
                {
                    if (vertices[i].Intersects(vertices[j]))
                    {
                        lst.Add(j);                        
                    }
                }
                for (int j = i + 1; j < count; j++)
                {
                    if (vertices[i].Intersects(vertices[j]))
                    {
                        lst.Add(j);
                    }
                }
                result[i] = lst.ToArray();
            }
            return result;
        }
    }
}
