﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.Collections.Generic;
using System.Globalization;
using adHoc;

namespace adHocTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            var g = new Graph(4);
            var matrix = new int[][] {
                new int[] {2},
                new int[] {0, 2},
                new int[] {1}
            };
            Console.WriteLine(g.Radius(matrix)); // 40
            //Assert.IsTrue(g.Radius(matrix) > 1e-5);
        }
        [TestMethod]
        public void TestMethod2()
        {
            var gen = new NetworkGenerator(100, 4);
            //var gen = new NetworkGenerator(100, 4);
            var v = gen.CreateVertex(50, true) as Vertex3D;
            Console.WriteLine("{0} {1} {2}", v.X, v.Y, v.Z);
        }
        [TestMethod]
        public void TestNetworkGeneration()
        {
            int count = 10;
            var gen = new NetworkGenerator(50, count);
            var matrix = gen.Generate(30, 50, false);
            for (int i = 0; i < count; i++)
            {
                Console.Write("# ");
                for (int j = 0; j < matrix[i].Count(); j++)
                {
                    Console.Write(matrix[i][j] + " ");
                }
                Console.WriteLine();
            }
        }
        [TestMethod]
        public void TestRadius()
        {
            int count = 10;
            var gen = new NetworkGenerator(50, count);
            var matrix = gen.Generate(10, 25, true);
            for (int i = 0; i < count; i++)
            {
                Console.Write("# ");
                for (int j = 0; j < matrix[i].Count(); j++)
                {
                    Console.Write(matrix[i][j] + " ");
                }
                Console.WriteLine();
            }
            Console.WriteLine("Radius = " + new Graph(count).Radius(matrix));
        }
        [TestMethod]
        public void TestRadius2()
        {
            var matrix = new int[][]
            {
                new int[] {1},
                new int[] {2},
                new int[] {3},
                new int[] {4},
                new int[] {5},
                new int[] {6},
                new int[] {},
            };
            Console.WriteLine(new Graph(7).Radius(matrix));
        }
        [TestMethod]
        public void TestDistances() 
        {
            var matrix = new int[][]
            {
                new int[] {1},
                new int[] {0, 2},
                new int[] {1, 3},
                new int[] {2, 4},
                new int[] {3, 5},
                new int[] {4, 6},
                new int[] {5},
            };
            var g = new Graph(7);
            Console.WriteLine(g.DistancesFrom(matrix, 0).Aggregate("", (acc, cur) => acc + " " + cur));
            Console.WriteLine(Enumerable.Range(0, 6)
                .Select(v => g.DistancesFrom(matrix, v).Max())
                .Aggregate("", (acc, cur) => acc + " " + cur.ToString())
            );
            Console.WriteLine(g.Radius(matrix));
            //Assert.Fail("TO DEBUG!!!");
            //Console.WriteLine(new Graph(7)..Aggregate("", (acc, cur) => acc + " " + cur));
            //Console.WriteLine(new Gr)
        }
        [TestMethod]
        public void TestTime()
        {
            var ts = new TimeSpan(2, 33, 44);
            Console.WriteLine(ts.ToString(@"hh\:mm\:ss"));
        }
    }
}
